#!/bin/bash

filename=entrada.dat
nlines=$1
incrementOfX=$2

rm -f entrada.dat

if [ $# -eq 2 ]; then
	lines=""
	for ((i = 0; i < nlines; i++)); do
		x=$(bc <<< "$i*$incrementOfX")
		fx=$(bc <<< "$x*2")
		lines="${lines}${x}, ${fx}\n" 
	done
	echo -e $lines >> entrada.dat
else
	echo "gendata.sh [nlines] [incrementOfX]"
fi
