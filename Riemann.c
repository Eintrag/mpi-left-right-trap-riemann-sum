# include <mpi.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>
	  
typedef struct { double x, fx;} x_fx;
double riemannLeft(double xi, double xiMinus1, double fxiMinus1);
double riemannRight(double xi, double xiMinus1, double fxi);
double riemannTrapezoidal(double xi, double xiMinus1, double fxi, double fxiMinus1);
int x_fxFromFile (int n, char *fileName, x_fx *tuples);
void calculateSums(double *Sizq, double *Sder, double *Strap, x_fx *tuples, int dp);
void printResults(double SizqTtl, double SderTtl, double StrapTtl, double elapsed_time_send_data, double elapsed_time);

int main ( int argc, char *argv[] ){
  int id, p, n;
  x_fx *tuples;
  double Sizq = 0, Sder = 0, Strap = 0; // individual values
  double SizqTtl = 0, SderTtl = 0, StrapTtl = 0; // total values
  MPI_Status status;
  double elapsed_time_calc, elapsed_time_send_data;
  MPI_Init ( &argc, &argv );
  MPI_Comm_size ( MPI_COMM_WORLD, &p );
  MPI_Comm_rank ( MPI_COMM_WORLD, &id );
  MPI_Datatype MPI_XFX; 
  MPI_Type_contiguous(2, MPI_DOUBLE, &MPI_XFX);
  MPI_Type_commit(&MPI_XFX);
  MPI_Barrier(MPI_COMM_WORLD);
  if(id == 0){
	n = atoi(argv[1]);
    tuples = (x_fx *) malloc (sizeof(x_fx)*n);
    n = x_fxFromFile (n, argv[2], tuples);
    elapsed_time_send_data = MPI_Wtime ( );  
  }
  /* if the file had less than n lines, n is nlines,
    let the other processors know the true value for n
  */
  MPI_Bcast ( &n, 1, MPI_INT, 0, MPI_COMM_WORLD );
  int dp = n/p + (id != p - 1) + ((id == p - 1) * (n%p));
  if (id !=0 ) {
    /* Only allocates the part that it uses, not the complete vector
      extra sizeof(x_fx) is to allow the additional value 
      from the next part of the vector
    */
    tuples = (x_fx *) malloc (sizeof(x_fx)*dp);    
  }
  if (p > 1){ // Send and receive only if more than 1 processor
    if (id == 0){
      int i;
      for(i = 1; i < p - 1; i++){
        MPI_Send(&tuples[i*(n/p)],dp,MPI_XFX,i,0,MPI_COMM_WORLD);
      }
      /* Last processor does not receive the first element of the 
	  next slice of data as there are no more slices. Last processor
	  receives the data that could not be divided (remainder).
      */
      MPI_Send(&tuples[(p-1)*(n/p)],dp-1+n%p,
		MPI_XFX,p-1,0,MPI_COMM_WORLD);
    }
    else{
      MPI_Recv(tuples, dp, MPI_XFX, 0, 0, MPI_COMM_WORLD, &status);
    }
  }
  if( id == 0){
    /* Quoting the requirements:
      "El tiempo de ejecución a calcular, será el transcurrido a partir 
      de recibir todos los procesos sus datos."
      For that reason, elapsed_time_send_data will consider 
      the time used to send and receive the data (before calc)
    */
    elapsed_time_send_data = MPI_Wtime()-elapsed_time_send_data;
    elapsed_time_calc = MPI_Wtime ( );  
  }
  calculateSums(&Sizq, &Sder, &Strap, tuples, dp);
  MPI_Reduce(&Sizq,&SizqTtl,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  MPI_Reduce(&Sder,&SderTtl,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  MPI_Reduce(&Strap,&StrapTtl,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  MPI_Finalize ( );
  if(id == 0){
    printResults(SizqTtl,SderTtl,StrapTtl,elapsed_time_send_data,elapsed_time_calc);
  }
  return 0;
}  

int x_fxFromFile (int n, char *fileName, x_fx *tuples){
  // returns number of read lines
  char line[128];
  FILE * file;
  file = fopen(fileName, "r");
  int nLinesRead = 0;
  while (nLinesRead < n){
    if(fgets(line, sizeof(line), file)){
      char *pt;
      pt = strtok (line,",");
      double num = atof(pt);
      int vectorPosition = nLinesRead;
      pt = strtok (NULL, ",");
      tuples[vectorPosition].x = num;

      num = atof(pt);
      pt = strtok (NULL, ",");
      tuples[vectorPosition].fx = num;

      nLinesRead++;
    } 
    // Won't read anything else if already reached n lines
    else break; 
  }
  fclose(file);
  return nLinesRead;
}

void calculateSums(double *Sizq, double *Sder, double *Strap, x_fx *tuples, int dp){
  int i;
  for(i = 1; i < dp; i++){ 
    *Sizq += riemannLeft(tuples[i].x, tuples[i-1].x, tuples[i-1].fx);
	*Sder += riemannRight(tuples[i].x, tuples[i-1].x, tuples[i].fx);
    *Strap += riemannTrapezoidal(tuples[i].x, tuples[i-1].x, tuples[i].fx, tuples[i-1].fx);
  }
}

void printResults(double SizqTtl, double SderTtl, double StrapTtl, double elapsed_time_send_data, double elapsed_time_calc){
  elapsed_time_calc = MPI_Wtime() - elapsed_time_calc;
  printf ( "Sizq=%lf\nSder=%lf\nSTrap=%lf\n", SizqTtl, SderTtl, StrapTtl);
  printf ( "Tiempo para enviar y recibir datos=%lf segundos\n", elapsed_time_send_data );
  printf ( "Tiempo máximo de ejecución=%lf segundos\n", elapsed_time_calc );
}

double riemannLeft(double xi, double xiMinus1, double fxiMinus1){
  return fxiMinus1 * (xi - xiMinus1);
}
double riemannRight(double xi, double xiMinus1, double fxi){
  return fxi * (xi - xiMinus1);
}
double riemannTrapezoidal(double xi, double xiMinus1, double fxi, double fxiMinus1){
  return ((fxiMinus1 + fxi)/2) * (xi - xiMinus1);
}