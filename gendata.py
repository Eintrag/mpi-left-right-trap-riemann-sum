#!/usr/bin/python
import sys
if(len(sys.argv) != 4):
	print "python gendata.py [nlines] [incrementOfX] [fileName]"
	sys.exit()
nlines=int(sys.argv[1])
incrementOfX=float(sys.argv[2])
f=open(sys.argv[3], 'w')
lines = []
for i in xrange(0, int(nlines)):
	x=i*incrementOfX
	fx=x*2
	f.write("{},{}\n".format(x, fx)) 
	#python gestiona el buffer por si mismo
f.close()