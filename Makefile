build:
	mpicc -Wall -o riemann Riemann.c

run:
	mpirun -np 1 riemann 2000 entrada.dat.big
	mpirun -np 2 riemann 2000 entrada.dat.big
	mpirun -np 3 riemann 2000 entrada.dat.big
	mpirun -np 4 riemann 2000 entrada.dat.big
	mpirun -np 5 riemann 2000 entrada.dat.big
	mpirun -np 10 riemann 2000 entrada.dat.big
	
	mpirun -np 1 riemann 200000 entrada.dat.big
	mpirun -np 2 riemann 200000 entrada.dat.big
	mpirun -np 3 riemann 200000 entrada.dat.big
	mpirun -np 4 riemann 200000 entrada.dat.big
	mpirun -np 5 riemann 200000 entrada.dat.big
	mpirun -np 10 riemann 200000 entrada.dat.big
	
	mpirun -np 1 riemann 2000000 entrada.dat.big
	mpirun -np 2 riemann 2000000 entrada.dat.big
	mpirun -np 3 riemann 2000000 entrada.dat.big
	mpirun -np 4 riemann 2000000 entrada.dat.big
	mpirun -np 5 riemann 2000000 entrada.dat.big
	mpirun -np 10 riemann 2000000 entrada.dat.big
	
	mpirun -np 1 riemann 50000000 entrada.dat.big
	mpirun -np 2 riemann 50000000 entrada.dat.big
	mpirun -np 3 riemann 50000000 entrada.dat.big
	mpirun -np 4 riemann 50000000 entrada.dat.big
	mpirun -np 5 riemann 50000000 entrada.dat.big
	mpirun -np 10 riemann 50000000 entrada.dat.big
	
	mpirun -np 1 riemann 100000000 entrada.dat.big
	mpirun -np 2 riemann 100000000 entrada.dat.big
	mpirun -np 3 riemann 100000000 entrada.dat.big
	mpirun -np 4 riemann 100000000 entrada.dat.big
	mpirun -np 5 riemann 100000000 entrada.dat.big
	mpirun -np 10 riemann 100000000 entrada.dat.big

clean:
	rm -f riemann
	rm -rf *~ 

all: clean build run